package build18.aam.smartpillbottle;

import android.support.v4.app.ListFragment;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends ListFragment {

    private View mainView;

    public MainActivityFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        mainView = inflater.inflate(R.layout.fragment_main, container, false);

        return mainView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setListAdapter(null);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        JSONArray pillListRaw = ((MainActivity) getActivity()).getCurrentInfo().optJSONArray("pill_list");
        ArrayList<JSONObject> pillList = new ArrayList<JSONObject>();

        try {
            for (int i = 0; i < pillListRaw.length(); i++) {
                pillList.add(pillListRaw.getJSONObject(i));
            }
        } catch (JSONException j) {
            Log.d("POPULATE_LIST", "Could not fetch JSON");
        }

        DemoOverviewAdapter da = new DemoOverviewAdapter(pillList);
        setListAdapter(da);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity ma = (MainActivity) getActivity();
                JSONObject info = ((DemoOverviewAdapter) getListAdapter()).getItem(position);
                ma.showPillInfo(info);
            }
        });


        FloatingActionButton fab = (FloatingActionButton) mainView.findViewById(R.id.addPillButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }



    private class DemoOverviewAdapter extends BaseAdapter {

        private ArrayList<JSONObject> items;

        public DemoOverviewAdapter(ArrayList<JSONObject> i) {
            items = i;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public long getItemId(int position) {
            return 1;
        }

        @Override
        public JSONObject getItem(int position) {
            return items.get(position);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.overview_item, parent, false);
            }
            TextView pillName = (TextView)convertView.findViewById(R.id.pillNameView);
            JSONObject pillInfo = getItem(position);
            try {
                pillName.setText(pillInfo.getString("name"));
            }catch (JSONException j) {
                pillName.setText("ERROR");
            }

            ImageView problemView = (ImageView)convertView.findViewById(R.id.infoImageView);
            problemView.setImageResource(android.R.drawable.ic_lock_idle_alarm);

            TextView pillCount = (TextView)convertView.findViewById(R.id.pillCountView);
            pillCount.setText("1/3");

            return convertView;
        }
    }
}
