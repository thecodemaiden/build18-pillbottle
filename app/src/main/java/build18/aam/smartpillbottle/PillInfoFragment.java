package build18.aam.smartpillbottle;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import build18.aam.smartpillbottle.dummy.DummyContent.DummyItem;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PillInfoFragment extends Fragment {

    // TODO: Customize parameter argument names
        private static final String ARG_PILL_JSON = "pillInfo";
    // TODO: Customize parameters
    private JSONObject pillToDisplay;
    private ListView listView;

   // private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PillInfoFragment() {
    }

    // TODO: Customize parameter initialization

    public static PillInfoFragment newInstance(String jString) {
        PillInfoFragment fragment = new PillInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PILL_JSON, jString);
        fragment.setArguments(args);
        return fragment;
    }

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pillinfo_list, container, false);


        if (getArguments() != null) {
            ArrayList<JSONObject> allTimes = new ArrayList<JSONObject>();

            try {
                String jString = getArguments().getString(ARG_PILL_JSON);
                pillToDisplay = new JSONObject(jString);
                JSONArray rawTimes = pillToDisplay.getJSONArray("times");

                int nTimes = rawTimes.length();
                for (int i=0; i<nTimes; i++) {
                    allTimes.add(rawTimes.getJSONObject(i));
                }

            } catch (JSONException j) {
                pillToDisplay = new JSONObject();
            }

            View header = inflater.inflate(R.layout.pill_info_header, null);
            TextView pillName = (TextView)header.findViewById(R.id.pillInfoName);
            pillName.setText(pillToDisplay.optString("name", "ERROR"));

            listView = (ListView)view.findViewById(android.R.id.list);
            listView.addHeaderView(header);
            
            DemoPillInfoAdapter da = new DemoPillInfoAdapter(allTimes);
            listView.setAdapter(da);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mListener = null;
    }

    private class DemoPillInfoAdapter extends BaseAdapter {

        private ArrayList<JSONObject> items;

        public DemoPillInfoAdapter(ArrayList<JSONObject> i) {
            items = i;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public long getItemId(int position) {
            return 1;
        }

        @Override
        public JSONObject getItem(int position) {
            return items.get(position);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_pillinfo, parent, false);
            }
            JSONObject o = getItem(position);
            Button start = (Button) convertView.findViewById(R.id.startTime);
            start.setText(o.optString("start", "ERR"));

            Button end = (Button) convertView.findViewById(R.id.endTime);
            end.setText(o.optString("end", "ERR"));

            return convertView;
        }
    }

}
