package build18.aam.smartpillbottle;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private JSONObject currentInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadData();

        Fragment firstFragment = new MainActivityFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentHolder, firstFragment);
        ft.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
       // loadData();
    }

    public JSONObject getCurrentInfo() {
        return currentInfo;
    }

    private void loadData()
    {
        String infoFilePath = "info.json";
      //  String infoFilePath = getFilesDir()+ File.separator + "info.json";

        try {
            FileInputStream fis = openFileInput(infoFilePath);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            fis.close();
            currentInfo = new JSONObject(sb.toString());
        } catch (IOException e) {
            currentInfo = null;
        } catch (JSONException j) {
            currentInfo = null;
        }

        if (currentInfo == null) {
            // make up a file
            currentInfo = new JSONObject();
            JSONArray times = new JSONArray();
            try {
                JSONObject bpPill = new JSONObject();
                JSONObject pillTime = new JSONObject();
                pillTime.put("start", "0600").put("end", "1000");
                times.put(pillTime);
                pillTime.put("start", "1300").put("end", "1700");
                times.put(pillTime);
                pillTime.put("start", "2000").put("end", "0000");
                times.put(pillTime);

                bpPill.put("times", times);
                bpPill.put("btAddress", "tbd");
                bpPill.put("name", "Blood Pressure");
                bpPill.put("num", 1); // one pill each time

                JSONArray allPills = new JSONArray();
                allPills.put(bpPill);
                currentInfo.put("pill_list", allPills);

                Log.d("JSON_LOAD", currentInfo.toString());
                FileOutputStream fos = openFileOutput(infoFilePath, MODE_PRIVATE);
                fos.write(currentInfo.toString(2).getBytes());
            } catch (JSONException j) {
                Log.d("JSON_LOAD", "Exception creating JSON");
            } catch (IOException e) {
                // oh dear.
            }
        }
    }

    public void showPillInfo(JSONObject pillInfo) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Log.d("PILL TRANSITION", currentInfo.toString());
        Fragment pillInfoFragment = PillInfoFragment.newInstance(pillInfo.toString());
        ft.replace(R.id.fragmentHolder, pillInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void updatePillInfo(JSONObject info){
        //TODO: insert/replace/in general info
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
